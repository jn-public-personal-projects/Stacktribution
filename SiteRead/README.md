## Running the StackExchange Site Changing Modification

### Python dependencies needed within SiteRead Folder

- [pick](https://github.com/wong2/pick)
- [httplib2](https://github.com/httplib2/httplib2)

Install them with the following commands:

```
wget https://raw.githubusercontent.com/pypa/get-pip/main/public/get-pip.py
python3 get-pip.py
pip install pick
pip install httplib2
```