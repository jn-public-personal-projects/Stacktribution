# Copyright (c) 2023 Juan Nevares
# Licensed under the MIT License

import re
import httplib2
from pick import pick

#
# @author Henno Brandsma <https://stackoverflow.com/users/342544/henno-brandsma>
# @copyright 2019 Henno Brandsma
# @license CC BY-SA 4.0
# @see {@link https://stackoverflow.com/questions/54940269/request-or-httplib2-request-taking-too-long-to-return-status-code|Request (or httplib2.request) taking too long to return status code}
#

with open ('./StackDomains.txt', 'r') as domainTextFile:

  amtLines = domainTextFile.readlines()
  lineCount = 0
  domainOptions = []

  userIdInput = input("Enter the Stack Web Site's User ID: ")  
  userIdInputString = str(userIdInput)

  title = 'Select the StackExhange Site to use: '

  for line in amtLines:
    currentString = amtLines[lineCount]
    domainOptions.append(currentString.strip("\n"))
    lineCount+=1

  domainOption, index = pick(domainOptions, title)

  urlName = ("https://" + domainOption + ".com/users/" + userIdInputString)

  http = httplib2.Http()
  resp = http.request(urlName)[0]
  print(resp.status)

  domainResponse = http.request(urlName)[0]
  while(domainResponse.status!=200):
    print("Invalid url")
    userIdInput = input("Please enter a valid StackExhange Site User ID: ")
    userIdInputString = str(userIdInput)
    urlName = ("https://" + domainOption + ".com/users/" + userIdInputString)
    domainResponse = http.request(urlName)[0]
    if (domainResponse.status==200):
      print("urlName  " + urlName + "  is valid") 

with open('../src/services/Utils/StackUtils.ts', 'r+') as stackUtilFile:

  stackRead = stackUtilFile.read()
  regexResult = re.findall("stackoverflow", stackRead)

  userIdReplace = ("const userId = '" + userIdInputString + "';")
  domainOptionReplace = ("const domain = '" + domainOption + "';")

  stackRead = re.sub(r"const userId =.+", userIdReplace, stackRead)
  stackRead = re.sub(r"const domain =.+", domainOptionReplace, stackRead)

  stackUtilFile.seek(0, 0)
  stackUtilFile.write(stackRead)
  stackUtilFile.truncate()

print("SiteRead Script has ended. Current site selected is:")
print("https://" + domainOption + ".com")
print("Please refresh browser and apply the Answer ID.")
