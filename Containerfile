# Download node base image debian

FROM node:18.15.0-buster-slim

# Disable prompt during Packages Installation
#
# @author Ohad Schneider <https://stackoverflow.com/users/67824/ohad-schneider>
# @copyright 2016 Ohad Schneider
# @license CC BY-SA 3.0
# @see {@link https://stackoverflow.com/a/35976127/1248177|Is it possible to answer dialog questions when installing under docker?}

ARG DEBIAN_FRONTEND=noninteractive

RUN apt update -y \
    && apt install -y apt-utils apt-transport-https python3 python3-distutils wget

RUN rm -rvf /var/lib/apt/lists/*

RUN wget https://raw.githubusercontent.com/pypa/get-pip/main/public/get-pip.py
RUN python3 get-pip.py

WORKDIR /Stacktribution
COPY . /Stacktribution

# Download and install pip from MIT licensed repo
# Repo: https://github.com/pypa/get-pip
# File from Repo: https://github.com/pypa/get-pip/blob/main/public/get-pip.py
# Repo License: https://github.com/pypa/get-pip/blob/main/LICENSE.txt

RUN pip install pick
RUN pip install httplib2

# Environment variable regarding nodejs 17+ issue.
#
# @author ninja_456 <https://stackoverflow.com/users/18037552/ninja-456>
# @copyright 2022 ninja_456
# @license CC BY-SA 4.0
# @see {@link https://stackoverflow.com/a/70863115/1248177|What is --openssl-legacy-provider in Node.js v17?}

ENV NODE_OPTIONS=--openssl-legacy-provider

RUN npm install
RUN npm install --save-dev @types/jquery
RUN npx next telemetry disable && npx browserslist@latest --update-db && npm run build 

EXPOSE 3000/udp
EXPOSE 3000/tcp

CMD ["npm", "run", "dev"]