export const AppConfig = {
  site_name: 'Stacktribution',
  title: 'Stacktribution',
  description:
    "A tiny webapp to generate proper attribution to a StackExchange Site's answer.",
  locale: 'en',
};
